## TechAssessment

There is a API  which receives data from the database and return form data in endpoint */api/v1/runner/{runnerId}/form-data*
Pass the runnerId as the parameter so that frontend can access the details from endpoint relevant to runner ID

## Tables 

There are tables tbm_meetings, tbm_races, tbm_runners, tbm_form_data, tbm_form_last_runs that handles various data coulmns relevant to meetings, races, runners, form data and last runs details of particular runner respectively.

## Repositories

This utilizes repository pattern. Each repository implements the interface named ApiRepositoryInterface. Repositories are injected to ApiController which fetch the runner data to endpoint from function *getFormData($id)*.


### Setup

## Prerequisites
-Git
-Composer
-Xampp

## clone the repository
Clone the repository to local environment. *git clone <reposritory_link>*

## install composer
Install all the dependencied using composer. *composer install*

## Copy env
Copy the example.env file and make the required configuration changes in the .env file

## Database migrations
Run the database migration. *php artisan migrate*
Make sure you set the correct database connection information before running the migrations

## Seed data
Run the database seeder. *php artisan db:seed*