<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbm_meetings')->insert([
            [   'id'=>'1',
                'external_id' => "sandown_vic",
                'name' => 'sandown'
            ],
            [
                'id'=>'2',
                'external_id' => "newcastle_qld",
                'name' => 'newcastle'
            ],

        ]);
        DB::table('tbm_races')->insert([
            [   'id'=>'1',
                'external_id' => "race3",
                'name' => 'race3',
                'meeting_id'=>'1'
            ],
            [   
                'id'=>'2',
                'external_id' => "race4",
                'name' => 'race4',
                'meeting_id'=>'1'
        ],
            [   'id'=>'3',
                'external_id' => "race1",
                'name' => 'race1',
                'meeting_id'=>'2'
            ],
            [   'id'=>'4',
                'external_id' => "race2",
                'name' => 'race2',
                'meeting_id'=>'2'
            ],

        ]);
    }
}
