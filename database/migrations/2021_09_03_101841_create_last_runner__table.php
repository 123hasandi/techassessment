<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLastRunnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbm_form_last_runs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('runner_id');
            $table->string('placing');
            $table->string('margin');
            $table->string('venue');
            $table->string('class');
            $table->foreign('runner_id')->references('id')->on('tbm_runners')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_runner_');
    }
}
