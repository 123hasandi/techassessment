<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LastRuns extends Model
{
    protected $table = "tbm_form_last_runs";
}
