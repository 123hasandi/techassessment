<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormData extends Model
{
    
    protected $table="tbm_form_data";

    public function runnerName(){
        return $this->hasOne(Runners::class,'id','runner_id');
    }
}
