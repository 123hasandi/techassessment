<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Runners extends Model
{
   
    protected $table = "tbm_runners";

    public function runnerLastRuns(){
        return $this->hasMany(lastRuns::class,'runner_id','id');
    }

    public function runnerFormData(){
        return $this->hasMany(formData::class,'runner_id','id');
    }
}
