<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\FormDataRepository;
use App\Repositories\RunnersRepository;

class ApiController extends Controller
{
    protected $formDataRepository;
    protected $runnersRepository;

    public function  __construct(FormDataRepository $formDataRepository,RunnersRepository $runnersRepository){
            $this->formDataRepository = $formDataRepository;
            $this->runnersRepository = $runnersRepository;
    }

    public function getFormData($id){
       $formData = $this->formDataRepository->getData($id);
       $rName = $formData->runnerName->name;
       $runner = $this->runnersRepository->getData($id);

       $lastRuns = array();
       foreach ($runner->runnerLastRuns as $runs){
           $lastRuns[] = $runs->toArray();
       }

       $data = [
           'runner name' => $rName ,
           'runner' =>  $formData->toArray() ,
           'lastRuns' => $lastRuns,
        ];

      return json_encode($data);
    }
    
 

}
