<?php

namespace App\Repositories;
use App\Models\FormData;


class FormDataRepository implements ApiRepositoryInterface
{

    protected $model;

    public function __construct(FormData $model){
        $this->model = $model;
    }

    public function getData($id){
        $formData = $this->model->with(['runnerName:name,id'])->select('runner_id','age','sex','color')->where('runner_id', $id)->first();
        return $formData;
    }



}