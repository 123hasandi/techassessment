<?php

namespace App\Repositories;
use App\Models\Runners;


class RunnersRepository implements ApiRepositoryInterface{

    protected $model;

    public function __construct(Runners $model){
        $this->model = $model;
    }

    public function getData($id){
        $runner = $this->model->where('id',$id)->first();
        return $runner;
    }



}