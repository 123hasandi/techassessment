<?php

namespace App\Repositories;

interface ApiRepositoryInterface
{
    public function getData($attribute);
}